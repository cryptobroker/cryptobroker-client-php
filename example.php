<?php
require_once('lib/cryptobroker-client.php');

try {
  $client = new CryptoBrokerClient(array(
                  'access_key' => 'ACCESS_KEY',
                  'secret_key' => 'SECRET_KEY'
            ));

  // Public
  // var_dump($client->get('/api/v2/market_list'));
  // var_dump($client->get('/api/v2/tickers'));
  // var_dump($client->get('/api/v2/order_book',['market' => 'btcusd']));
  // var_dump($client->get('/api/v2/depth',['market' => 'btcusd']));
  // var_dump($client->get('/api/v2/chart_data',['market' => 'btcusd']));
  // var_dump($client->get('/api/v2/chart_data_with_pending_trades',['market' => 'btccad', 'trade_id' => 1]));
  // var_dump($client->get('/api/v2/timestamp'));
  // var_dump($client->get('/api/v2/market_trades',['market' => 'btccad']));

  // Private Account
  // var_dump($client->get('/api/v2/account'));
  // var_dump($client->get('/api/v2/account/deposit',['txid' => 'dbd7f65dd224e8fe530dc2361e764a4ba63813bb3e4bb323803ccd2d760e3fa5']));
  // var_dump($client->get('/api/v2/account/deposits'));
  // var_dump($client->get('/api/v2/account/deposit_address',['currency' => 'btc']));
  // var_dump($client->get('/api/v2/account/order',['id' => 38]));
  // var_dump($client->get('/api/v2/account/orders',['market' => 'btccad']));
  // var_dump($client->get('/api/v2/account/trades',['market' => 'btccad']));

  // Private Trade
  // var_dump($client->post('/api/v2/trade/order',['market' => 'btccad', 'side' => 'buy', 'volume' => '1', 'price' => '10']));
  // var_dump($client->post('/api/v2/trade/cancel',['id' => 35]));
  // var_dump($client->post('/api/v2/trade/cancel_all',['side' => 'buy']));

  // FIX ME
  // var_dump($client->post('/api/v2/trade/multi_order',['market' => 'btcusd', 'orders' => [{'side' => 'buy', 'volume' => '1', 'price' => '1'}, {'side' => 'buy', 'volume' => '1', 'price' => '2'}]]));
  // var_dump($client->get('/api/v2/tickers/btccad'));

}
catch (Exception $e) {
  die($e);
}

?>
