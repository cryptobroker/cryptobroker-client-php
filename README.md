cryptobroker-client-php
==============

A simple PHP client for Crypto Trade Site CryptoBroker.io

Quick example
=============

```php

<?php
require_once('lib/cryptobroker-client.php');

try {
  $client = new CryptoBrokerClient(array(
                  'access_key' => 'Your access key',
                  'secret_key' => 'Your secret key'
            ));
  // var_dump($client->get('/api/v2/market_list'));
  // var_dump($client->get('/api/v2/account'));
  // var_dump($client->post('/api/v2/trade/cancel',['id' => 1]));
}
catch (Exception $e) {
  die($e);
}

```

API Document
=============

[API_v2](https://trade.cryptobroker.io/documents/api_v2)
